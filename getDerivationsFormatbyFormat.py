import rucio.client
client = rucio.client.didclient
cl = rucio.client.didclient.DIDClient

import pyAMI.client #import AMIClient                                                                                                                                                                          
import pyAMI.atlas.api as AtlasAPI
client = pyAMI.client.Client('atlas')
AtlasAPI.init()

import pickle



def isBlacklistedFormat(fname):
    if fname in ["DAOD_2L2QHSG2",
                 "DAOD_2LHSG2",
                 "DAOD_4LHSG2",
                 "DAOD_BCID1",
                 "DAOD_BCID2",
                 "DAOD_BCID3",
                 "DAOD_BCID4",
                 "DAOD_DAPR0",
                 "DAOD_DAPR2",
                 "DAOD_EGAMMA1",
                 "DAOD_EGAMMA2",
                 "DAOD_EGAMMA3",
                 "DAOD_EGAMMA4",
                 "DAOD_EGAMMA5",
                 "DAOD_EGLOOSE",
                 "DAOD_EGZ",
                 "DAOD_ELLOOSE18",
                 "DAOD_EMU",
                 "DAOD_HSG1",
                 "DAOD_HSG2",
                 "DAOD_IDNCB",
                 "DAOD_IDPIXLUMI",
                 "DAOD_IDTIDE",
                 "DAOD_IDTR1",
                 "DAOD_IDTRKLUMI",
                 "DAOD_IDTRKVALID",
                 "DAOD_JPSIHSG2",
                 "DAOD_JPSIMUMU",
                 "DAOD_ONIAMUMU",
                 "DAOD_ONIAMUMUHI",
                 "DAOD_PASSTHR",
                 "DAOD_PHYSVAL",
                 "DAOD_PIXELVALID",
                 "DAOD_RNDM",
                 "DAOD_RPVLL",
                 "DAOD_SCTVALID",
                 "DAOD_SUSYEGAMMA",
                 "DAOD_SUSYJETS",
                 "DAOD_SUSYMUONS",
                 "DAOD_TAUMUH",
                 "DAOD_TRTVALID",
                 "DAOD_UPSIMUMU",
                 "DAOD_WENU",
                 "DAOD_WMUNU",
                 "DAOD_ZEE",
                 "DAOD_ZEEGAMMA",
                 "DAOD_ZMUMU",
                 "DAOD_ZMUMUGAMMA",
                 "DAODM",
                 "DAODM_HSG1",
                 "DAOD_2L2QHSG2",
                 "DAOD_2LHSG2",
                 "DAOD_4LHSG2",
                 "DAOD_BCID1",
                 "DAOD_BCID2",
                 "DAOD_BCID3",
                 "DAOD_BCID4",
                 "DAOD_EGAM",
                 "DAOD_EGAMMA1",
                 "DAOD_EGAMMA2",
                 "DAOD_EGAMMA3",
                 "DAOD_EGAMMA4",
                 "DAOD_EGAMMA5",
                 "DAOD_EGLOOSE",
                 "DAOD_EGZ",
                 "DAOD_ELLOOSE18",
                 "DAOD_EMU",
                 "DAOD_HSG1",
                 "DAOD_HSG2",
                 "DAOD_IDNCB",
                 "DAOD_IDPIXLUMI",
                 "DAOD_IDTIDE",
                 "DAOD_IDTRKLUMI",
                 "DAOD_IDTRKVALID",
                 "DAOD_JPSIHSG2",
                 "DAOD_JPSIMUMU",
                 "DAOD_ONIAMUMU",
                 "DAOD_ONIAMUMUHI",
                 "DAOD_PASSTHR",
                 "DAOD_RNDM",
                 "DAOD_RPVLL",
                 "DAOD_SCTVALID",
                 "DAOD_SUSYEGAMMA",
                 "DAOD_SUSYJETS",
                 "DAOD_SUSYMUONS",
                 "DAOD_TAUMUH",
                 "DAOD_TRTVALID",
                 "DAOD_UPSIMUMU",
                 "DAOD_WENU",
                 "DAOD_WMUNU",
                 "DAOD_ZEE",
                 "DAOD_ZEEGAMMA",
                 "DAOD_ZMUMU",
                 "DAOD_ZMUMUGAMMA",
                 "2L2QHSG2",
                 "2LHSG2",
                 "4LHSG2",
                 "BCID1",
                 "BCID2",
                 "BCID3",
                 "BCID4",
                 "EGAM",
                 "EGAMMA1",
                 "EGAMMA2",
                 "EGAMMA3",
                 "EGAMMA4",
                 "EGAMMA5",
                 "EGLOOSE",
                 "EGZ",
                 "ELLOOSE18",
                 "EMU",
                 "HSG1",
                 "HSG2",
                 "IDNCB",
                 "IDPIXLUMI",
                 "IDTIDE",
                 "IDTRKLUMI",
                 "IDTRKVALID",
                 "JPSIHSG2",
                 "JPSIMUMU",
                 "ONIAMUMU",
                 "ONIAMUMUHI",
                 "PASSTHR",
                 "RNDM",
                 "RPVLL",
                 "SCTVALID",
                 "SUSYEGAMMA",
                 "SUSYJETS",
                 "SUSYMUONS",
                 "TAUMUH",
                 "TRTVALID",
                 "UPSIMUMU",
                 "WENU",
                 "WMUNU",
                 "ZEE",
                 "ZEEGAMMA",
                 "ZMUMU",
                 "ZMUMUGAMMA"
                 ]:
        return True
    return False


def getFormat(deriv):
    format = ''.join([i for i in deriv if not i.isdigit()])
    if "_" in format: format = format.split("_")[-1]
    return format


def getCamp(ds):
    if 'r9364'  in ds: return "mc16a"
    if 'r9781'  in ds: return "mc16c"
    if 'r10201' in ds: return "mc16d"
    if 'r10724' in ds: return "mc16e"
    return ""


nom = AtlasAPI.list_types(client,patterns=["DAOD_%"])

allformats = []

info = {}
tids = []

scope = "data"
for n in nom:
    if isBlacklistedFormat(n['name']): continue
    format = "DAOD_"+getFormat(n['name'])
    if "HIGGD" in format:
        format = "DAOD_HIGG"
    elif "LCALO" in format:
        format = "DAOD_L1CALO"
    if not format in allformats:
        allformats.append(format)

for af in allformats:

    #if not "SUSY" in af: continue

    print "Doing %s" %af

    if "data" in scope:
        amids   = AtlasAPI.list_datasets(client, patterns=["data15_%."+af+"%_p%"], fields=['atlas_release','project','total_size','prodsys_status'],  atlas_release='AthDerivation_21.2.%.0', prodsys_status='ALL EVENTS AVAILABLE')
        amids  += AtlasAPI.list_datasets(client, patterns=["data16_%."+af+"%_p%"], fields=['atlas_release','project','total_size','prodsys_status'],  atlas_release='AthDerivation_21.2.%.0', prodsys_status='ALL EVENTS AVAILABLE')
        amids  += AtlasAPI.list_datasets(client, patterns=["data17_%."+af+"%_p%"], fields=['atlas_release','project','total_size','prodsys_status'],  atlas_release='AthDerivation_21.2.%.0', prodsys_status='ALL EVENTS AVAILABLE')
        amids  += AtlasAPI.list_datasets(client, patterns=["data18_%."+af+"%_p%"], fields=['atlas_release','project','total_size','prodsys_status'],  atlas_release='AthDerivation_21.2.%.0', prodsys_status='ALL EVENTS AVAILABLE')
    elif "mc" in scope:
        amids  = AtlasAPI.list_datasets(client, patterns=["mc16_%."+af+"%_p%"], fields=['atlas_release','project','total_size','prodsys_status'],  atlas_release='AthDerivation_21.2.%.0', prodsys_status='ALL EVENTS AVAILABLE')

    info[af] = {}

    nds = len(amids)
    n = 1
    for ldn in amids:
        #if n > 40: break
        if n%20 == 0: print "%i/%i" %(n,nds)

        ds  = ldn['ldn']
        #print ds
        ptag  = ds.split("_")[-1]
        format = ds.split(".")[4]
        if "data" in scope:
            project  = ldn['project']
            try:
                size = float(ldn['total_size'])
            except:
                print "ERROR \t Can  not get size for %s !!" %ds
                size = 0
        else:
            project = getCamp(ds)
            if not project:
                print "ERROR \t Could not find campaign for %s" %ds
                project = "other"
            dslist = cl.list_content(cl(),ds.split(".")[0],ds)
            size = 0
            for dsls in dslist:
                tid = dsls["name"]
                if tid in tids: 
                    print "TID %s already registered" %tid
                    continue
                tids.append(tid)
                metadata = cl.get_metadata(cl(), ds.split(".")[0], tid)
                size     += float(metadata["bytes"]) if metadata["bytes"] != None else 0.0

        release = ldn['atlas_release']



        ancestors = AtlasAPI.get_dataset_prov(client, ds)
        ancDs = ancestors['node']
        for ads in ancDs:
            distance = ads['distance']
            if (distance=="-1"):
                ancName = ads['logicalDatasetName']
                try:
                    parent = AtlasAPI.list_datasets(client,patterns=ancName,fields=['total_size'])#,'events'])
                except:
                    break
                parentSize = (parent[0])['total_size']
                if (parentSize=='NULL'):
                    print 'Warning: NULL size for ',ancName  
                    break
                parentSize = float(parentSize)
                #if (float(parentSize)==0.0):
                #    print 'Warning: zero size for ',ancName
                #    break
                #parentNEvents = (parent[0])['events'] 

        #print ldn['prodsys_status']
        

        if not format in info[af].keys():
            info[af][format] = {}
        if not project in info[af][format].keys():
            info[af][format][project] = {}
        if not ptag in info[af][format][project].keys():
            info[af][format][project][ptag] = {"size":0.0,"parentsize":0.0,"nds":0}
        info[af][format][project][ptag]["size"] += size
        info[af][format][project][ptag]["parentsize"] += parentSize
        info[af][format][project][ptag]["nds"] += 1

        n += 1

picklename = "Oct_20_2022_%s.pickle" %scope
out_s = open(picklename, 'wb')
pickle.dump(info, out_s)
out_s.close()



    




