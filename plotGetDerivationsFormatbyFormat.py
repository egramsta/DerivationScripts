from ROOT import *
import pickle
import sys
gStyle.SetOptStat(0)
gStyle.SetOptTitle(0)


alldaods = {}
if "deletionlist_latest_DAILYCHECK" in sys.argv[1]:
    lines = [line.rstrip('\n') for line in open(sys.argv[1])]
    for l in lines:
        if not l: continue
        if "TOTAL" in l: continue
        sp = l.split()
        alldaods[sp[0].strip()] = {"size":float(sp[1])*1.0e12}
else:
    in_s = open(sys.argv[1])
    alldaods = pickle.load(in_s)
    in_s.close()



h = {}
ibin = 0
totsize = 0.0
for fm in sorted(alldaods.keys()):
    ibin += 1
    print fm
    for key2 in alldaods[fm].keys():
        print key2
        if not 'size' in h.keys():
            h['size'] = TH1F("h_%s"%key2,"h_%s"%key2,len(alldaods.keys()),0,len(alldaods.keys()))
        #print(fm,key2)
        for scope in alldaods[fm][key2].keys():
            for ptag in alldaods[fm][key2][scope].keys():
                print(alldaods[fm][key2][scope][ptag].keys())
                h['size'].AddBinContent(ibin,alldaods[fm][key2][scope][ptag]['size'])
                totsize += alldaods[fm][key2][scope][ptag]['size']

    for key in h.keys():
        h[key].GetXaxis().SetBinLabel(ibin,fm.replace("DAOD_",""))



histmidl = h["size"].Clone("histmidl")

nformats = 0
for i in range(1,histmidl.GetNbinsX()+1):
    if histmidl.GetBinContent(i) != 0: nformats += 1

h["size_sorted"] = TH1F("h_size_sorted","h_size_sorted",nformats,0,nformats)

t = {}
for i in range(1,histmidl.GetNbinsX()+1):
    maxbin = histmidl.GetBinContent(histmidl.GetMaximumBin())
    if maxbin == 0: break
    h["size_sorted"].SetBinContent(i,maxbin)
    h["size_sorted"].GetXaxis().SetBinLabel(i,h["size"].GetXaxis().GetBinLabel(histmidl.GetMaximumBin()))
    histmidl.SetBinContent(histmidl.GetMaximumBin(),0.0)

c = {}
for key in sorted(h.keys()):
    c[key] = TCanvas("c_%s"%key,"c_%s"%key,1000,600)
    c[key].SetLogy()
    c[key].SetBottomMargin(0.15)
    h[key].SetFillColor(kAzure)
    if "size" in key: 
        h[key].Scale(1./1e12)
    h[key].Draw("hist")
    h[key].GetYaxis().SetTitle("Size [TB]")
    h[key].GetXaxis().SetTitle("Format")
    h[key].GetXaxis().SetTitleOffset(2.0)

    if "size" in key: 
        t[key] = TLatex()
        t[key].DrawLatex(41,41,"Total size = %.2f TB"%(totsize/1e12))
        c[key].Update()

print "Total = %.2f"%(totsize/1e12)


sys.exit()


if 0:
    h = {}

    ibin = 0
    totsize = 0.0
    for fm in sorted(alldaods.keys()):
        ibin += 1
        print fm
        for key in alldaods[fm].keys():
            print key
            for key2 in alldaods[fm][key].keys():
                print key2
                if not key2 in h.keys():
                    h[key2] = TH1F("h_%s"%key2,"h_%s"%key2,len(alldaods.keys()),0,len(alldaods.keys()))
                for key3 in alldaods[fm][key][key2].keys():
                    print key3
                    h[key2].AddBinContent(ibin,alldaods[fm][key][key2][key3]/1e12)
                    totsize += alldaods[fm][key][key2][key3]

        for key in h.keys():
            h[key].GetXaxis().SetBinLabel(ibin,fm)


c = TCanvas("c","c",1000,600)
c.SetLogy()
stack = THStack()
col = [kGreen,kRed,kBlue,kOrange,kMagenta,kBlack]
nk = 0
leg = TLegend(0.729226,0.723629,0.958453,0.972574)
leg.SetBorderSize(0)
leg.SetTextFont(42)
leg.SetTextSize(0.04)
leg.SetTextColor(1)
leg.SetFillColor(0)
leg.SetLineColor(0)
for key in sorted(h.keys()):
    h[key].SetFillColor(col[nk])
    stack.Add(h[key])
    leg.AddEntry(h[key],key,"f")
    print nk
    nk += 1
stack.Draw()
stack.GetYaxis().SetTitle("Size [TB]")
stack.GetXaxis().SetTitle("Format")
stack.GetXaxis().SetTitleOffset(1.5)
leg.Draw()



