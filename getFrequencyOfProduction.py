import sys
import rucio.client
from datetime import datetime, timedelta
import pickle

CheckDate = datetime.now() - timedelta(days=120)
print(CheckDate)

client = rucio.client.didclient
cl = rucio.client.didclient.DIDClient
import pyAMI.client

import pyAMI.atlas.api as AtlasAPI
#client = pyAMI.client.Client('atlas')                                                                                                                                                                          
# Eirik July 2019: the IN2P3 of AMI is broken on slc6, needs to use the atlas replica:                                                                                                                          
client = pyAMI.client.Client(['atlas-replica', 'atlas'])
AtlasAPI.init()


def isBlacklistedFormat(fname):
    if fname in ["DAOD_IDTR2",
                 "DAOD_CALJET",
                 "DAOD_EVTPICK",
                 "DAOD_2L2QHSG2",
                 "DAOD_2LHSG2",
                 "DAOD_4LHSG2",
                 "DAOD_BCID1",
                 "DAOD_BCID2",
                 "DAOD_BCID3",
                 "DAOD_BCID4",
                 "DAOD_DAPR0",
                 "DAOD_DAPR2",
                 "DAOD_EGAMMA1",
                 "DAOD_EGAMMA2",
                 "DAOD_EGAMMA3",
                 "DAOD_EGAMMA4",
                 "DAOD_EGAMMA5",
                 "DAOD_EGLOOSE",
                 "DAOD_EGZ",
                 "DAOD_ELLOOSE18",
                 "DAOD_EMU",
                 "DAOD_HSG1",
                 "DAOD_HSG2",
                 "DAOD_IDNCB",
                 "DAOD_IDPIXLUMI",
                 "DAOD_IDTIDE",
                 "DAOD_IDTR1",
                 "DAOD_IDTRKLUMI",
                 "DAOD_IDTRKVALID",
                 "DAOD_JPSIHSG2",
                 "DAOD_JPSIMUMU",
                 "DAOD_ONIAMUMU",
                 "DAOD_ONIAMUMUHI",
                 "DAOD_PASSTHR",
                 "DAOD_PHYSVAL",
                 "DAOD_PIXELVALID",
                 "DAOD_RNDM",
                 "DAOD_RPVLL",
                 "DAOD_SCTVALID",
                 "DAOD_SUSYEGAMMA",
                 "DAOD_SUSYJETS",
                 "DAOD_SUSYMUONS",
                 "DAOD_TAUMUH",
                 "DAOD_TRTVALID",
                 "DAOD_UPSIMUMU",
                 "DAOD_WENU",
                 "DAOD_WMUNU",
                 "DAOD_ZEE",
                 "DAOD_ZEEGAMMA",
                 "DAOD_ZMUMU",
                 "DAOD_ZMUMUGAMMA",
                 "DAODM",
                 "DAODM_HSG1",
                 "DAOD_2L2QHSG2",
                 "DAOD_2LHSG2",
                 "DAOD_4LHSG2",
                 "DAOD_BCID1",
                 "DAOD_BCID2",
                 "DAOD_BCID3",
                 "DAOD_BCID4",
                 "DAOD_EGAM",
                 "DAOD_EGAMMA1",
                 "DAOD_EGAMMA2",
                 "DAOD_EGAMMA3",
                 "DAOD_EGAMMA4",
                 "DAOD_EGAMMA5",
                 "DAOD_EGLOOSE",
                 "DAOD_EGZ",
                 "DAOD_ELLOOSE18",
                 "DAOD_EMU",
                 "DAOD_HSG1",
                 "DAOD_HSG2",
                 "DAOD_IDNCB",
                 "DAOD_IDPIXLUMI",
                 "DAOD_IDTIDE",
                 "DAOD_IDTRKLUMI",
                 "DAOD_IDTRKVALID",
                 "DAOD_JPSIHSG2",
                 "DAOD_JPSIMUMU",
                 "DAOD_ONIAMUMU",
                 "DAOD_ONIAMUMUHI",
                 "DAOD_PASSTHR",
                 "DAOD_RNDM",
                 "DAOD_RPVLL",
                 "DAOD_SCTVALID",
                 "DAOD_SUSYEGAMMA",
                 "DAOD_SUSYJETS",
                 "DAOD_SUSYMUONS",
                 "DAOD_TAUMUH",
                 "DAOD_TRTVALID",
                 "DAOD_UPSIMUMU",
                 "DAOD_WENU",
                 "DAOD_WMUNU",
                 "DAOD_ZEE",
                 "DAOD_ZEEGAMMA",
                 "DAOD_ZMUMU",
                 "DAOD_ZMUMUGAMMA",
                 "2L2QHSG2",
                 "2LHSG2",
                 "4LHSG2",
                 "BCID1",
                 "BCID2",
                 "BCID3",
                 "BCID4",
                 "EGAM",
                 "EGAMMA1",
                 "EGAMMA2",
                 "EGAMMA3",
                 "EGAMMA4",
                 "EGAMMA5",
                 "EGLOOSE",
                 "EGZ",
                 "ELLOOSE18",
                 "EMU",
                 "HSG1",
                 "HSG2",
                 "IDNCB",
                 "IDPIXLUMI",
                 "IDTIDE",
                 "IDTRKLUMI",
                 "IDTRKVALID",
                 "JPSIHSG2",
                 "JPSIMUMU",
                 "ONIAMUMU",
                 "ONIAMUMUHI",
                 "PASSTHR",
                 "RNDM",
                 "RPVLL",
                 "SCTVALID",
                 "SUSYEGAMMA",
                 "SUSYJETS",
                 "SUSYMUONS",
                 "TAUMUH",
                 "TRTVALID",
                 "UPSIMUMU",
                 "WENU",
                 "WMUNU",
                 "ZEE",
                 "ZEEGAMMA",
                 "ZMUMU",
                 "ZMUMUGAMMA"]:
        return True
    return False


scope = sys.argv[1].split(",")

allformats = []
                                                                                                                                                                                                    
nom = AtlasAPI.list_types(client,patterns=["DAOD_%"])
for n in nom:
    if isBlacklistedFormat(n['name']):
        continue
    form = n['name']
    if "LCALO" in form:
        form = "DAOD_L1CALO"
    if not form in allformats:
        print(form)
        allformats.append(form)




alldaods = {}
infodic = {}
nf = 1
for f in allformats:
    #f = "DAOD_STDM4"
    print("Doing %s, format %i/%i" %(f,nf,len(allformats)))
    #if not "SUSY" in f: continue
    #if nf < 134: 
    #    nf += 1
    #    continue
    nf += 1
    alldaods[f] = []
    for s in scope:
        try:
            alldaods[f].append(cl.list_dids(cl(),s,{"name":"%s.*.deriv.%s.*_p*"%(s,f),"is_open":0},'dataset')) #,"closed_at":CheckDate
        except:
            print("Problems getting list for %s.*.deriv.%s.*_p*" %(s,f))
            continue
    for ad in alldaods[f]:
        i = 0
        while True:
            i += 1
            if i%20 == 0: print("Doing %i" %i)
            try:
                cont = ad.next()
                #print cont
                metadata = cl.get_metadata(cl(), cont.split(".")[0], cont)
                ExpectedDate = metadata["closed_at"]
                #print(ExpectedDate)
                if CheckDate > ExpectedDate:
                    #print("Too old")
                    continue
                else:
                    #print("Within time period")
                    ds = "_".join(cont.split("_")[:-3])
                    #print ds
                    ptag = cont.split("_")[-3]
                    #print ds, ptag
                    if not ds in infodic.keys():
                        infodic[ds] = {}
                    if not ptag in infodic[ds].keys():
                        infodic[ds][ptag] = {"closed_at":ExpectedDate}
            except:
                print("Failed")
                break
        #break
    #if nf > 50: break

with open('ds_closed_%s.pickle'%"_".join(scope), 'wb') as handle:
    pickle.dump(infodic, handle, protocol=pickle.HIGHEST_PROTOCOL)
