from ROOT import *
import pickle
import sys, os

import pyAMI.client

import pyAMI.atlas.api as AtlasAPI
#client = pyAMI.client.Client('atlas')                                                                                                                                                                          
# Eirik July 2019: the IN2P3 of AMI is broken on slc6, needs to use the atlas replica:                                                                                                                          
client = pyAMI.client.Client(['atlas-replica', 'atlas'])
AtlasAPI.init()


infile = sys.argv[1]

h_deltat = TH1F("h_deltat","Time between production of data sets",80,0,160)
h_deltat_samecache = TH1F("h_deltat_samecache","Time between production of data sets (same cache)",80,0,160)

h_deltat_formats = {}#TH1F("h_deltat","Time between production of data sets",80,0,160)


with open(infile, 'rb') as handle:
    d = pickle.load(handle)


infodic = {}

nds = 0
for ds in d.keys():
    nds += 1
    print(ds)
    #if not "HIGG1D1" in ds: continue
    print(d[ds].keys())
    if len(d[ds].keys()) > 1:
        dts = []
        pts = []
        for p in d[ds].keys():
            print(p)
            dts.append(d[ds][p]["closed_at"])
            pts.append(p)
        dts = sorted(dts)
        for i in range(len(dts)-1):
            if not pts[i] in infodic.keys():
                info1 = AtlasAPI.get_ami_tag(client,pts[i])
                infodic[pts[i]] = info1[0]
            if not pts[i+1] in infodic.keys():
                info2 = AtlasAPI.get_ami_tag(client,pts[i+1])
                infodic[pts[i+1]] = info2[0]
            
            samecache = False
            if infodic[pts[i]]['cacheName'] == infodic[pts[i+1]]['cacheName']:
                samecache = True


            #print dts[i]
            #print dts[i+1]
            deltaT = dts[i+1] - dts[i]
            print ("deltaT = ",deltaT)
            if deltaT.days < 20 and not samecache:# and "SUSY1." in ds:
                print("ds = ",ds)
                print("ptags = ", d[ds])
                fm = ds.split(".")[4]
                if not fm in h_deltat_formats:
                    h_deltat_formats[fm] = TH1F("h_deltat_%s"%fm,"Time between production of data sets (%s)"%fm,30,0,30)
                h_deltat_formats[fm].Fill(deltaT.days)
            if not samecache:
                h_deltat.Fill(deltaT.days)
            else:
                h_deltat_samecache.Fill(deltaT.days)

    else:
        h_deltat.Fill(155)


print("Total number of derivations produced: ", nds)


st = THStack()
h_deltat_samecache.SetFillColor(kAzure)
st.Add(h_deltat_samecache)
h_deltat.SetFillColor(kRed)
st.Add(h_deltat)
st.Draw("hist")
st.GetXaxis().SetTitle("Days between re-derivation")
st.GetYaxis().SetTitle("Number of data sets")


if 0:#"data" in infile:
    c2 = TCanvas("c2","c2",1)
    st2 = THStack()
    color = [kAzure,kSpring,kMagenta,kOrange,kCyan,kYellow,kBlack]
    i = 0
    leg = TLegend(0.6,0.5,0.85,0.85)
    for fm in h_deltat_formats:
        h_deltat_formats[fm].SetFillColor(color[i])
        h_deltat_formats[fm].SetLineColor(color[i])
        st2.Add(h_deltat_formats[fm])
        leg.AddEntry(h_deltat_formats[fm],fm.replace("DAOD_",""),"lf")
        i += 1
    st2.Draw("hist")
    st2.GetXaxis().SetTitle("Days between re-derivation")
    st2.GetYaxis().SetTitle("Number of data sets")
    leg.Draw()
